<?php

namespace Concat\Templating\Tests;

use Concat\Templating\Engine;
use Concat\Filesystem\Directory;
use Concat\Filesystem\File;
use Concat\Config\Config;

trait AssetTagTest
{
    private $engine;

    private function buildConfig($cache, $compress, $dependencies)
    {
        return new Config([
            "debug" => true,
            "paths" => [
                "root"              => __DIR__,
                "assets"            => '/samples', // relative to root, can't be array
                "templates"         => '/templates/source', // relative to root, could be array

                "cache" => [
                    "assets"        => 'cache', // relative to root
                    "templates"     => 'cache', // relative to root
                ],
            ],
            "assets" => [
                "cache"             => $cache,
                "compress"          => $compress,
                "dependencies"      => $dependencies,
            ],
        ]);
    }

    private $mtimes = [];

    public function setUp()
    {
        $this->mtimes = [];
        clearstatcache();

        //touch all files so that their mtimes are equivalent
        foreach (Directory::iterator(__DIR__."/samples") as $file) {
            $this->mtimes[$file->getPathname()] = $file->getMTime();
            touch($file, 123);
        }
    }

    public function tearDown()
    {
        Directory::delete(__DIR__."/cache");
        foreach ($this->mtimes as $file => $mtime) {
            touch($file, $mtime);
        }
    }

    private function templateEquals($cache, $compress, $dependencies = false)
    {
        $name = $this->getTagName();

        $config = $this->buildConfig($cache, $compress, $dependencies);

        $this->engine = new Engine($config);

        $output = $this->engine->render($name.".html");
        $file = __DIR__."/templates/expected/$name/$name";

        Directory::createParent($file);

        if ($cache) {
            $file .= "_cached";
        }
        if ($compress) {
            $file .= "_compressed";
        }
        if ($dependencies) {
            $file .= "_dep";
        }

        file_put_contents("$file.html", $output);
        $expected = File::getContents("$file.html");

        $this->assertEquals($expected, $output, $file);
    }

    public function testTag()
    {
        $this->templateEquals(false, false);
    }

    public function testTagCompressed()
    {
        $this->templateEquals(false, true);
    }

    public function testTagCached()
    {
        $this->templateEquals(true, false);
    }

    public function testTagCachedCompressed()
    {
        $this->templateEquals(true, true);
    }

    public function testDependencies()
    {
        if ($this->hasDependencies()) {
            $this->templateEquals(false, false, true);
        }
    }

    public function hasDependencies()
    {
        return false;
    }
}
