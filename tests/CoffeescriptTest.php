<?php

namespace Concat\Templating\Tests;

class CoffeescriptTest extends \PHPUnit_Framework_TestCase
{
    use AssetTagTest;

    public function getTagName()
    {
        return 'coffee';
    }
}
