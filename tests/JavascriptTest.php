<?php

namespace Concat\Templating\Tests;

class JavascriptTest extends \PHPUnit_Framework_TestCase
{
    use AssetTagTest;

    public function getTagName()
    {
        return 'js';
    }
}
