<?php

namespace Concat\Templating\Tests;

class JQueryTest extends \PHPUnit_Framework_TestCase
{
    use AssetTagTest;

    public function getTagName()
    {
        return 'jquery';
    }

    public function hasDependencies()
    {
        return true;
    }
}
