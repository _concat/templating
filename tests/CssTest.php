<?php

namespace Concat\Templating\Tests;

class CssTest extends \PHPUnit_Framework_TestCase
{
    use AssetTagTest;

    public function getTagName()
    {
        return 'css';
    }
}
