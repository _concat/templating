[![Author](http://img.shields.io/badge/author-@rudi_theunissen-blue.svg?style=flat)](https://twitter.com/rudi_theunissen)
[![Build Status](https://img.shields.io/travis/concat/templating/master.svg?style=flat)](https://travis-ci.org/concat/templating)
[![Software License](https://img.shields.io/packagist/l/concat/templating.svg?style=flat)](LICENSE.md)
[![Latest Version](https://img.shields.io/github/tag/concat/templating.svg?style=flat&label=release)](https://github.com/concat/templating/tags)
[![Total Downloads](https://img.shields.io/packagist/dt/concat/templating.svg?style=flat)](https://packagist.org/packages/concat/templating)
[![Version](https://img.shields.io/packagist/v/concat/templating.svg?style=flat)](https://packagist.org/packages/concat/templating)
[![Test Coverage](https://codeclimate.com/github/concat/templating/badges/coverage.svg?style=flat)](https://codeclimate.com/github/concat/templating)
[![Code Climate](https://codeclimate.com/github/concat/templating/badges/gpa.svg?style=flat)](https://codeclimate.com/github/concat/templating)

This package is compliant with [PSR-1], [PSR-2] and [PSR-4]. If you notice compliance oversights,
please send a patch via pull request.

[PSR-1]: https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-1-basic-coding-standard.md
[PSR-2]: https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md
[PSR-4]: https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-4-autoloader.md

## Install

Via Composer

``` bash
$ composer require concat/templating
```
