<?php

namespace Concat\Templating;

use function \Concat\Helpers\Paths\join_paths;
use Concat\Config\Config;

class Environment extends \Twig_Environment
{
    private $config;

    public function __construct(Config $config)
    {
        $this->config = $config;

        $options = [
            'cache' => $this->getTemplateCachePath(),
            'debug' => $config->get('debug', false),
        ];

        $loader = new Loader($config);

        //
        parent::__construct($loader, $options);

        //
        $extensions = [
            new Extensions\AssetTags\AssetTags($config),

        ];

        //
        $this->setExtensions($extensions);
    }

    public function getTemplateCachePath()
    {
        //
        $root = $this->config->get('paths.root', __DIR__);

        //
        $cache = $this->config->get('paths.cache.templates');

        return join_paths($root, $cache);
    }

    public function getConfig()
    {
        return $this->config;
    }
}
