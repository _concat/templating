<?php

namespace Concat\Templating;

use function Concat\Helpers\Classes\class_basename;

abstract class Extension extends \Twig_Extension
{
    public function __construct()
    {
    }

    // private function extract($container, $class, $options)
    // {
    //     if ($container) {
    //         //
    //         $methods = get_class_methods($container);

    //         // remove the constructor if present
    //         // faster than in_array?
    //         if (($key = array_search('__construct', $methods)) !== false) {
    //             unset($methods[$key]);
    //         }

    //         //
    //         return array_map(function ($name) use ($container, $class, $options) {

    //             //
    //             $callable = [$container, $name];

    //             return new $class($name, $callable, $options);
    //         }, $methods);
    //     }

    //     return [];
    // }

    // new Functions($this->config)
    // public function getFunctions()
    // {
    //     $container = $this->getFunctionContainer();
    //     $options = [
    //         'is_safe' => [
    //             'html'
    //         ]
    //     ];

    //     return $this->extract($container, \Twig_SimpleFunction::class, $options);
    // }

    // public function getFilters()
    // {
    //     $container = $this->getFilterContainer();
    //     $options = [];

    //     return $this->extract($container, \Twig_SimpleFilter::class, $options);
    // }

    public function getName()
    {
        return class_basename($this);
    }
}
