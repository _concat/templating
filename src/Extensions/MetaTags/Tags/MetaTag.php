<?php

namespace Concat\Templating\Extensions\MetaTags\Tags;

abstract class MetaTag extends \Twig_TokenParser
{

    public function getClosingTag()
    {
        return "end" . $this->getTag();
    }

    public function parse(\Twig_Token $token)
    {
        $stream = $this->parser->getStream();

        // parse attributes if any
        $attributes = $this->parseAttributes($stream);

        $line = $token->getLine();
        $tag = $this->getTag();
        $node = $this->getNodeClass();

        return new $node($attributes, $line, $tag);
    }

    private function parseAttributes($stream, $allowNull = false)
    {
        $attributes = [];

        // it's possible that current is already block end type
        // so while there are still tokens before the end
        while (!$stream->test(\Twig_Token::BLOCK_END_TYPE)) {

            // get the key
            $key = $stream->expect(\Twig_Token::NAME_TYPE);
            //$value = null;

            if($allowNull){

                $value = null;

                if ($stream->look()->test(\Twig_Token::OPERATOR_TYPE, "=")) {

                    // pass the '='
                    $stream->next();

                    // pass the value
                    $stream->next();

                    // get the value
                    $value = $this->parsePrimaryExpression();
                }

            } else {
                $stream->expect(\Twig_Token::OPERATOR_TYPE, "=");

                // get the value
                $value = $this->parsePrimaryExpression();
            }

            $attributes[] = [$key, $value];
        }

        $stream->expect(\Twig_Token::BLOCK_END_TYPE);


        return new \Twig_Node($attributes);
        return $attributes;
    }
