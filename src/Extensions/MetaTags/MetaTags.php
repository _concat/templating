<?php

namespace Concat\Templating\Extensions\MetaTags;

use Concat\Templating\Extension;

// use function Concat\Helpers\Paths\join_paths;
// use function Concat\Helpers\Paths\check_extension;


// use Assetic\Asset\AssetCollection;
// use Assetic\Asset\FileAsset;
// use Assetic\Asset\StringAsset;
// // use Assetic\Filter\CssMinFilter;
// use Assetic\Factory\AssetFactory;

// use Concat\Filesystem\Instance\File;
// use Concat\Filesystem\Instance\Path;

class MetaTags extends Extension
{
    public function __construct($config)
    {
    }

    public function getTokenParsers()
    {
        return [
            new Tags\Facebook(),
            new Tags\Twitter(),
            new Tags\Google(),
        ];
    }
}
