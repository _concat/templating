<?php

namespace Concat\Templating\Extensions\AssetTags;

use Concat\Templating\Extension;

// use Concat\Templating\Tags\Css;
// use Concat\Templating\Tags\Less;
// use Concat\Templating\Tags\JQuery;

use function Concat\Helpers\Paths\join_paths;
use Assetic\Asset\AssetCollection;
use Assetic\Asset\FileAsset;
use Assetic\Asset\StringAsset;
// use Assetic\Filter\CssMinFilter;
use Assetic\Factory\AssetFactory;
use Concat\Filesystem\Instance\File;
use Concat\Filesystem\Instance\Path;

class AssetTags extends Extension
{
    private $adapters = [];

    public function __construct($config)
    {
        // parent::__construct($config);
        $this->debug            = $config->get("debug",                 false);
        $this->root             = $config->get("paths.root",            __DIR__);
        $this->assets           = $config->get("paths.assets",          "");
        $this->cache            = $config->get("paths.cache.assets",    "cache");
        $this->compress         = $config->get("assets.compress",       false);
        $this->dependencies     = $config->get("assets.dependencies",   true);
        $this->cache_enabled    = $config->get("assets.cache",          false);
        $this->jquery_version   = $config->get("assets.jquery_version", "1.11.1");

        $this->jquery_loaded    = false;
    }

    public function getTokenParsers()
    {
        return [
            new Tags\Css(),
            new Tags\Less(),
            new Tags\JQuery(),
            new Tags\Javascript(),
            new Tags\Scss(),
            new Tags\Sass(),
            new Tags\Coffeescript(),
        ];
    }

    public function getJQueryCdnUrl()
    {
        $file = "jquery-".$this->jquery_version;

        //
        if ($this->compress) {
            $file .= '.min';
        }

        return "https://code.jquery.com/$file.js";
    }

    private function parseFileSource($file, $extension)
    {
        $absolutes = [];
        $relatives = [];
        $fails = [];

        foreach ((array) $file as $filename) {
            $relative = new Path($filename);

            $relative->setExtension($extension);

            $absolute = new Path($this->root, $this->assets, $relative);

            if ($absolute->isReadable()) {
                $absolutes[] = $absolute;
                $relatives[] = trim($relative, "/");
            } else {
                $fails[] = $filename;
            }
        }

        return [$absolutes, $relatives, $fails];
    }

    // private function getAdapter($class)
    // {
    //     if (!isset($this->adapters[$class])) {
    //         //
    //         $adapter = new $class();

    //         $adapter->setOption('compress', $this->compress);

    //         $adapter->setCacheDirectory(join_paths($this->root, $this->cache));

    //         // cache hashes should be be affected by the root dir
    //         $adapter->setRelativePath($this->root);

    //         $this->adapters[$class] = $adapter;
    //     }

    //     return $this->adapters[$class];
    // }

    private function getAssetFilters($node)
    {
        // $filters = $node->getAdditionalFilters();
        // if($this->compress){
        //     $filters[] = $node->getCompressFilter();
        // }
        // return $filters;

       // var_dump($node::getFilters($this->compress));

        return $node::getFilters($this->compress);
    }

    public function compileString($string, $node)
    {
        $asset = new StringAsset($string, $this->getAssetFilters($node));

        return $asset->dump();
    }

    public function compileStringToCache($string, $node)
    {
        // hash the input? maybe alongside the node class
        // that way there won't be confliting same inputs??
        // check if the hash exists in the cache dir
        // if it does, return that path
        // otherwise create the file
        // and then return that path.
        $asset = new StringAsset($string, $this->getAssetFilters($node));

        $extension = $node::getSourceExtension();

        $hash = $this->generateHash($string, $extension);

        return $this->cache($hash, $asset);
    }

    private function createAssetCollection($paths, $node)
    {
        $assets = [];

        $filters = $this->getAssetFilters($node);

        foreach ($paths as $path) {
            $assets[] = new FileAsset($path);
        }

        return new AssetCollection($assets, $filters);
    }

    public function compileFile($file, $node)
    {
        $extension = $node::getSourceExtension();

        list($abs, $rel, $err) = $this->parseFileSource($file, $extension);

        $res = null;

        if ($abs) {
            // there is at least 1 valid absolute path

            $collection = $this->createAssetCollection($abs, $node);

            $res = $collection->dump();
            //$res = $this->cache($hash, $collection, 'css');
        }

        return [$res, $rel, $err];
    }

    private function generateHash($source, $extension, $mtime = 0)
    {
        $data = [
            $this->compress,
            $source,
            $mtime,
        ];

        $hash = md5(json_encode($data));

        return $hash.".".$extension;
    }

    private function cache($hash, $asset)
    {
        $relative = new File($this->cache, $hash);

        $absolute = new File($this->root, $relative);

        if (!$absolute->exists()) {
            //
            $relative->createParent();

            //
            $relative->putContents($asset->dump());
        }

        return $relative;
    }

    public function compileFileToCache($file, $node)
    {
        $extension = $node::getSourceExtension();

        list($abs, $rel, $err) = $this->parseFileSource($file, $extension);

        $res = null;

        if ($abs) {
            // there is at least 1 valid absolute path
            $factory = new AssetFactory($this->root, $this->debug);

            $collection = $this->createAssetCollection($abs, $node);

            $mtime = $factory->getLastModified($collection);

            // now we can create a cache file for it,
            // check if it already exists etc
            // otherwise create it
            $hash = $this->generateHash($rel, $extension, $mtime);

            $res = $this->cache($hash, $collection);
        }

        return [$res, $rel, $err];
    }
}
