<?php

namespace Concat\Templating\Extensions\AssetTags\Tags;

class Javascript extends ScriptTag
{
    public function getTag()
    {
        return 'js';
    }
}
