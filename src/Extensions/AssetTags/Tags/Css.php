<?php

namespace Concat\Templating\Extensions\AssetTags\Tags;

class Css extends StyleTag
{
    public function getTag()
    {
        return 'css';
    }
}
