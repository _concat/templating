<?php

namespace Concat\Templating\Extensions\AssetTags\Tags;

use Concat\Templating\Extensions\AssetTags\Nodes\StyleNode;

abstract class StyleTag extends AssetTag
{
    protected function getNodeClass()
    {
        return StyleNode::class;
    }
}
