<?php

namespace Concat\Templating\Extensions\AssetTags\Tags;

use Concat\Templating\Extensions\AssetTags\Nodes\CoffeeNode;

class Coffeescript extends AssetTag
{
    protected function getNodeClass()
    {
        return CoffeeNode::class;
    }

    public function getTag()
    {
        return 'coffee';
    }
}
