<?php

namespace Concat\Templating\Extensions\AssetTags\Tags;

use Concat\Templating\Extensions\AssetTags\Nodes\LessNode;

class Less extends AssetTag
{
    protected function getNodeClass()
    {
        return LessNode::class;
    }

    public function getTag()
    {
        return 'less';
    }
}
