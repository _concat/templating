<?php

namespace Concat\Templating\Extensions\AssetTags\Tags;

use Concat\Templating\Extensions\AssetTags\Nodes\ScssNode;

class Scss extends AssetTag
{
    protected function getNodeClass()
    {
        return ScssNode::class;
    }

    public function getTag()
    {
        return 'scss';
    }
}
