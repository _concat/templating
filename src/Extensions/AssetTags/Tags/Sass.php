<?php

namespace Concat\Templating\Extensions\AssetTags\Tags;

use Concat\Templating\Extensions\AssetTags\Nodes\SassNode;

class Sass extends AssetTag
{
    protected function getNodeClass()
    {
        return SassNode::class;
    }

    public function getTag()
    {
        return 'sass';
    }
}
