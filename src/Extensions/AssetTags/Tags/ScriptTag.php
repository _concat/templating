<?php

namespace Concat\Templating\Extensions\AssetTags\Tags;

use Concat\Templating\Extensions\AssetTags\Nodes\ScriptNode;

abstract class ScriptTag extends AssetTag
{
    protected function getNodeClass()
    {
        return ScriptNode::class;
    }
}
