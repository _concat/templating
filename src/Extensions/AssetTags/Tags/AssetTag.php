<?php

namespace Concat\Templating\Extensions\AssetTags\Tags;

abstract class AssetTag extends \Twig_TokenParser
{
    abstract protected function getNodeClass();
    abstract public function getTag();

    public function getClosingTag()
    {
        return "end".$this->getTag();
    }

    private function parseAttributes($stream)
    {
        $attributes = [];

        $parser = $this->parser->getExpressionParser();
        // it's possible that current is already block end type
        // so while there are still tokens before the end
        while (!$stream->test(\Twig_Token::BLOCK_END_TYPE)) {
            // get the key
            $key = $stream->expect(\Twig_Token::NAME_TYPE)->getValue();

            $value = null;

            if ($stream->test(\Twig_Token::OPERATOR_TYPE, "=")) {
                // pass the '='
                $stream->next();

                // pass the value
                //$stream->next();

                // $token = $stream->getCurrent();

                $value = $parser->parsePrimaryExpression();

           //     var_dump(get_class($value));
                if (!($value instanceof \Twig_Node_Expression_Constant)) {
                    throw new \Twig_Error_Syntax(
                        'An attribute value must be a string or number.',
                        $token->getLine(),
                        $this->parser->getFilename()
                    );
                }

                $value = $value->getAttribute('value');

                // switch($token->getType()){
                //     case \Twig_Token::STRING_TYPE:
                //     case \Twig_Token::INTERPOLATION_START_TYPE:

                //         break;
                //     case \Twig_Token::NUMBER_TYPE:
                //         $value =  new \Twig_Node_Expression_Constant($token, $token->getLine());
                //         break;
                //     default:

                //     );

               // }
            }

            // if($value !== null){

            // }

            $attributes[$key] = $value;
        }

        $stream->expect(\Twig_Token::BLOCK_END_TYPE);

        return $attributes;
        //return new \Twig_Node($attributes);
    }

    private function parseFileSource($stream)
    {
        $parser = $this->parser->getExpressionParser();

        if ($stream->test(\Twig_Token::STRING_TYPE)) {
            // token is string so we can assume source file
            return $parser->parseStringExpression();
        }

        if ($stream->test(\Twig_Token::PUNCTUATION_TYPE)) {
            // token is array so we can assume multiple source files
            return $parser->parseArrayExpression();
        }

        //$stream->expect(\Twig_Token::BLOCK_END_TYPE);
    }

    private function parseStringSource($stream)
    {
        $source = $this->parser->subparse(function (\Twig_Token $token) {
            return $token->test($this->getClosingTag());
        }, true);

        $stream->expect(\Twig_Token::BLOCK_END_TYPE);

        return $source;
    }

    public function parse(\Twig_Token $token)
    {
        $stream = $this->parser->getStream();
        $nodes = [];

        // parse file source if any
        $source = $this->parseFileSource($stream);

        // parse attributes if any
        $attributes = $this->parseAttributes($stream);

        if ($source === null) {
            // parse content between tags
            $source = $this->parseStringSource($stream);
        }

        $line = $token->getLine();
        $tag = $this->getTag();
        $node = $this->getNodeClass();

        $nodes['source'] = $source;
        //$nodes['attrs'] = $attributes;

        return new $node($nodes, $attributes, $line, $tag);
    }

    // final public function getTag()
    // {
    //     return $this->getTagName();
    // }
}
