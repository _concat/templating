<?php

namespace Concat\Templating\Extensions\AssetTags\Tags;

use Concat\Templating\Extensions\AssetTags\Nodes\JQueryNode;

class JQuery extends AssetTag
{
    protected function getNodeClass()
    {
        return JQueryNode::class;
    }

    public function getTag()
    {
        return 'jquery';
    }
}
