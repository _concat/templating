<?php

namespace Concat\Templating\Extensions\AssetTags\Nodes;

use Assetic\Filter\LessFilter;

class LessNode extends StyleNode
{
    public static function getFilters($compress)
    {
        $filters = [];//parent::getFilters($compress);


        $less = new LessFilter();

        $less->setCompress($compress);

        // $less->setNodePaths([
        //    '/usr/lib/node_modules',
        // ]);

        $filters[] = $less;

        // if($compress){
        //     $filters[] = new CssMinFilter();
        // }

        return $filters;
    }

    public static function getSourceExtension()
    {
        return "less";
    }
}
