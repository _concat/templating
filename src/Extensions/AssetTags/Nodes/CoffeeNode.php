<?php

namespace Concat\Templating\Extensions\AssetTags\Nodes;

use Assetic\Filter\CoffeeScriptFilter;

class CoffeeNode extends ScriptNode
{
    public static function getFilters($compress)
    {
        $filters = parent::getFilters($compress);

        // $filters = [];

        // check node paths in config?


        $coffee = new CoffeeScriptFilter(
            // 'coffee',
            // 'node'
        );

        // $coffee->setNodePaths([
        //     '/usr/lib/node_modules',
        // ]);

        $coffee->setNoHeader(true);
        //$coffee->setBare(true);

        $filters[] = $coffee;

        // $coffee->setNodePaths([
        //     '/usr/local/lib/nodes_modules',
        //     '/usr/lib/nodes_modules',
        //     './node_modules/'
        // ]);


        // if($compress){
        //     $filters[] = new CssMinFilter();
        // }

        return $filters;
    }
}
