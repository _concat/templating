<?php

namespace Concat\Templating\Extensions\AssetTags\Nodes;

use Assetic\Filter\CssMinFilter;

class StyleNode extends AssetNode
{
    protected function getDefaultAttributes()
    {
        return [
            // rel only if it's a file
            "rel"   => "stylesheet",

            // HTML5 default
            "type"  => "text/css"
        ];
    }

    public static function getSourceExtension()
    {
        return "css";
    }

    public static function getCompiledExtension()
    {
        return "css";
    }

    // expects file
    protected function buildFileSource()
    {
        $this->line('echo "\n<link href=\"$file\" ";');
        $this->compileAttributes();
        $this->line('echo ">\n";');
    }

    // expect $compiled
    protected function buildTextSource()
    {
        $this->line('echo "\n<style ";');
        $this->compileAttributes('rel');
        $this->line('echo ">\n$body\n</style>\n";');
    }

    public static function getFilters($compress)
    {
        $filters = [];

        if ($compress) {
            $filters[] = new CssMinFilter();
        }

        return $filters;
    }
}
