<?php

namespace Concat\Templating\Extensions\AssetTags\Nodes;

use Assetic\Filter\Sass\ScssFilter;

class ScssNode extends StyleNode
{
    public static function getFilters($compress)
    {
        $filters = parent::getFilters($compress);

        // $filters = [];

        $filters[] = new ScssFilter();

        // if($compress){
        //     $filters[] = new CssMinFilter();
        // }

        return $filters;
    }
}
