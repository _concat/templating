<?php

namespace Concat\Templating\Extensions\AssetTags\Nodes;

use Assetic\Filter\Sass\SassFilter;

class SassNode extends StyleNode
{
    public static function getFilters($compress)
    {
        $filters = parent::getFilters($compress);

        // $filters = [];

        $filters[] = new SassFilter();

        // if($compress){
        //     $filters[] = new CssMinFilter();
        // }

        return $filters;
    }

    public static function getSourceExtension()
    {
        return "sass";
    }
}
