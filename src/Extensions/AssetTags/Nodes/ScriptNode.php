<?php

namespace Concat\Templating\Extensions\AssetTags\Nodes;

use Assetic\Filter\JSqueezeFilter;

class ScriptNode extends AssetNode
{
    protected function getDefaultAttributes()
    {
        return [
            // HTML5 default
            "type"  => "text/javascript"
        ];
    }

    public static function getSourceExtension()
    {
        return "js";
    }

    public static function getCompiledExtension()
    {
        return "js";
    }

    // source is a node
    protected function buildFileSource()
    {
        $this->line('echo "\n<script src=\"$file\" ";');
        $this->compileAttributes();
        $this->line('echo "></script>\n";');
    }

    // source is a node
    protected function buildTextSource()
    {
        $this->line('echo "\n<script ";');
        $this->compileAttributes();
        $this->line('echo ">\n$body\n</script>\n";');
    }

    public static function getFilters($compress)
    {
        $filters = [];

        if ($compress) {
            $filters[] = new JSqueezeFilter();
        }

        return $filters;
    }
}
