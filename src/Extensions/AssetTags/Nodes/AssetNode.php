<?php

namespace Concat\Templating\Extensions\AssetTags\Nodes;

abstract class AssetNode extends \Twig_Node
{
    private $source;

    public function __construct($nodes, $attributes, $line, $tag)
    {
        $this->source = $nodes['source'];
        parent::__construct($nodes, $attributes, $line, $tag);
    }

    abstract protected function getDefaultAttributes();

    public static function getFilters($compress)
    {
        return [];
    }

    public static function getSourceExtension()
    {
    }
    public static function getCompiledExtension()
    {
    }

    abstract protected function buildFileSource();
    abstract protected function buildTextSource();

    protected function stringifyAttributes($attributes)
    {
        $list = [];

        foreach ($attributes as $key => $value) {
            if ($value === null) {
                $list[] = $key;
            } elseif (is_bool($value)) {
                $list[] = $key."=".$value ? '"true"' : '"false"';
            } else {
                $list[] = $key."="."\"$value\"";
            }
        }

        return join(' ', $list);
    }

    protected function compileAttributes(...$exclusions)
    {
        $attributes = $this->getDefaultAttributes();

        foreach ($exclusions as $e) {
            unset($attributes[$e]);
        }

        foreach ($this->attributes as $key => $value) {
            // list($key, $value) = $attribute;
            if ($value === null) {
                $attributes[$key] = null;
            } else {
                $attributes[$key] = $value;
            }
        }

        // join the attrs
        $attributes = $this->stringifyAttributes($attributes);

        $this->line("echo '$attrs';");
    }

    /**
     * Compiles the node to PHP.
     *
     * @param Twig_Compiler $compiler A Twig_Compiler instance
     */
    public function compile(\Twig_Compiler $compiler)
    {
        $compiler->addDebugInfo($this);

        $this->compiler = $compiler;

        $this->storeNodeClass();
        $this->storeExtension();

        switch (get_class($this->source)) {
            case \Twig_Node_Text::class:
            case \Twig_Node::class:
                $this->compileSourceString();
                break;
            case \Twig_Node_Expression_Constant::class:
            case \Twig_Node_Expression_Array::class:
                $this->compileSourceFile();
                break;
        }
    }

    private function compileSourceFile()
    {
        $this->cacheEnabledConditional(
            [$this, 'compileFileToCache'],
            [$this, 'compileFile']
        );
    }

    private function compileSourceString()
    {
        // $body is now the text to compile
        $this->subcompileToVariable($this->source, 'source', true);

        $this->cacheEnabledConditional(
            [$this, 'compileStringToCache'],
            [$this, 'compileString']
        );
    }

    private function compileString()
    {
        $this->line('$body = $ext->compileString($source, $node);');
        $this->buildTextSource();
    }

    private function compileStringToCache()
    {
        $this->line('$file = $ext->compileStringToCache($source, $node);');
        $this->buildFileSource();
    }

    protected function storeNodeClass()
    {
        $this->line('$node = \''.get_class($this).'\';');
    }

    protected function storeExtension()
    {
        $this->line('$ext = $this->env->getExtension("AssetTags");');
    }

    protected function comment($comment)
    {
        $this->line('if ($this->env->isDebug()) {');
        $this->line('echo "<!-- " . '.$comment.' . " -->";');
        $this->line('}');
    }

    private function documentRelatives()
    {
        $this->line('if ($relatives) {');
        $this->comment('join(", ", $relatives)');
        $this->line('}');
    }

    private function compileFileToCache()
    {
        $this->subcompileToVariable($this->source, 'input');

        $this->line('$compiled = $ext->compileFileToCache($input, $node);');

        $this->line('list($file, $relatives, $fails) = $compiled;');

        $this->documentRelatives();

        $this->line('if($file){');
        $this->buildFileSource();
        $this->line('}');

        $this->buildFailures();
    }

    private function compileFile()
    {
        $this->subcompileToVariable($this->source, 'input');

        $this->line('$compiled = $ext->compileFile($input, $node);');

        $this->line('list($body, $relatives, $fails) = $compiled;');

        $this->documentRelatives();

        $this->line('if($body){');
        $this->buildTextSource();
        $this->line('}');

        $this->buildFailures();
    }

    private function buildFailures()
    {
        $this->line('foreach($fails as $file){');
        $this->comment('"\'$file\' could not be read as file"');
        $this->buildFileSource();
        $this->line('}');
    }

    protected function startBuffer()
    {
        $this->line('ob_start();');
    }

    protected function saveBuffer($name)
    {
        $this->line('$'.$name.' = ob_get_clean();');
    }

    protected function subcompileToVariable($node, $name, $buffer = false)
    {
        if ($buffer) {
            $this->startBuffer();
            $this->compiler->subcompile($node);
            $this->saveBuffer($name);
        } else {
            $this->compiler->write('$'.$name.' = ');
            $this->compiler->subcompile($node);
            $this->compiler->raw(";");
        }
    }

    protected function line($line)
    {
        $this->compiler->raw("$line\n");
    }

    private function cacheEnabledConditional(callable $if, callable $else)
    {
        $this->line('if($ext->cache_enabled){');
        $if();
        $this->line('} else {');
        $else();
        $this->line('}');
    }
}
