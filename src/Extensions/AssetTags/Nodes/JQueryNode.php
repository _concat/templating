<?php

namespace Concat\Templating\Extensions\AssetTags\Nodes;

class JQueryNode extends ScriptNode
{
    // source is a node
    protected function buildFileSource()
    {
        //
        $this->line('$save = $file;');

        //
        $this->loadJQuery();

        //
        $this->line('$file = $save;');

        parent::buildFileSource();
    }

    private function loadJQuery()
    {
        $this->line('if(!$ext->jquery_loaded && $ext->dependencies){');

            //
            $this->line('$file = $ext->getJQueryCdnUrl();');

            //
            $this->line('$ext->jquery_loaded = true;');

            //
            parent::buildFileSource();

        $this->line('}');
    }

    // source is a node
    protected function buildTextSource()
    {
        $this->loadJQuery();

        parent::buildTextSource();
    }
}
