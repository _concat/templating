<?php

namespace Concat\Templating;

class Engine
{
    private $environment;

    // private $config = [
    //     "paths" => [
    //         "root"              => __DIR__,
    //         "assets"            => '', // relative to root, can't be array
    //         "templates"         => '', // relative to root, could be array

    //         "cache" => [
    //             "assets"        => 'cache', // relative to root
    //             "templates"     => 'cache', // relative to root
    //         ],
    //     ],
    //     "cache"         => false,
    //     "compress"      => false
    // ];

    public function __construct($config)
    {
        $environment = new Environment($config);

        //
        $this->environment = $environment;
    }

    public function getEnvironment()
    {
        return $this->environment;
    }

    public function render($template, $context = [])
    {
        return $this->environment->render($template, $context);
    }

    public function display($template, $context = [])
    {
        return $this->environment->display($template, $context);
    }
}
