<?php

namespace Concat\Templating;

use function \Concat\Helpers\Paths\join_paths;
use Concat\Config\Config;

class Loader extends \Twig_Loader_Filesystem
{
    private $config;

    public function __construct($config)
    {
        $this->config = $config;

        parent::__construct($this->getTemplatePaths());
    }

    private function getTemplatePaths()
    {
        //
        $root = $this->config->get('paths.root', __DIR__);

        //
        $paths = (array) $this->config->get('paths.templates');

        //
        foreach ($paths as &$path) {
            $path = join_paths($root, $path);
        }

        return $paths;
    }
}
